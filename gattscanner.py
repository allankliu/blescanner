#!/usr/bin/env python3
#coding: utf-8

# https://github.com/IanHarvey/bluepy (Python interface to BLE on Linux)
# https://github.com/getsenic/gatt-python (Python GATT interface to BLE)
# https://blog.csdn.net/zhuo_lee_new/article/details/108649169

import gatt

class AnyDeviceManager(gatt.DeviceManager):
    def device_discovered(self, device):
        print("Discovered [{}] {}".format(device.mac_address, device.alias()))

manager = AnyDeviceManager(adapter_name="hci0")
manager.start_discovery()
manager.run()


