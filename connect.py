#!/usr/bin/env python3
#coding: utf-8

import gatt

from argparse import ArgumentParser

class AnyDevice(gatt.Device):
    def connect_succeeded(self):
        super().connect_succeeded()
        print("[{}] Connected".format(self.mac_address))

    def connect_failed(self, error):
        super().connect_failed(error)
        print("[{}] Connection failed: {}".format(self.mac_address, str(error)))

    def disconnect_succeeded(self):
        super().disconnect_succeeded()
        print("[{}] Disconnected".format(self.mac_address))

    def services_resolved(self):
        super().services_resolved()

        print("[%s] Resolved services" % (self.mac_address))
        print(self)
        for service in self.services:
            print("[%s]  Service [%s]" % (self.mac_address, service.uuid))
            for characteristic in service.characteristics:
                print("[%s]    Characteristic [%s]" % (self.mac_address, characteristic.uuid))


argparse = ArgumentParser(description="GATT Connect Demo")
argparse.add_argument('mac_address', help="Mac address to connect")
args = argparse.parse_args()
print(args)
print("Connecting...")

manager = gatt.DeviceManager(adapter_name="hci0")

device = AnyDevice(manager=manager, mac_address=args.mac_address)
device.connect()

manager.run()
