# An experimental BLE application on Linux

I met many issues during my development for BLE applications. This application is cloned from github and CSDN.
The code will be tested on x86 Linux box and Raspiberry Pi with a BLE USB dongle. 

## Experimental with bluetoothctl

- https://www.cnblogs.com/zjutlitao/p/9589661.html

## Pending Issues

- bluetoothctl list-attributes has nothing return.
- list services are also failed, not None is return.
- https://lore.kernel.org/all/1815601.YRP3FTkMZ6@ix/t/
- https://stackoverflow.com/questions/53209265/rpi3bluez-managed-to-connect-to-a-ble-device-but-cant-list-gatt-attributes
- https://waylonwalker.com/linux-bluetoothctl/
